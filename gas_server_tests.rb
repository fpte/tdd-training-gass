ENV['RACK_ENV'] = 'test'

require './gas_server'
require 'test/unit'
require 'rack/test'
require 'json'
require 'shoulda-context'

class GASServerTest < Test::Unit::TestCase
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  def setup
    # password: admin
    ENV['ADMIN_PASSWORD_SALT'] = "b5c6525c0812eb1b111a0c43d7371a3f"
    ENV['ADMIN_PASSWORD_HASH'] = "0c0df41b4d09b50a4086f6a514a3c8b66aca9d06d681e9bbb45270f5a14b330c7830685c0bb87bcafefbac9c1c99cb26644c0de487ade9040c1a7abc12b0d760"
    
    # TODO: use different method to change configuration without authenticating admin
    authorize "admin", "admin"
    get '/config/reset'
    authorize "wrong_user", "wrong_password"
  end
  
  def set_api_version(version)
    authorize "admin", "admin"
    get "/config/setversion/#{version}"
    authorize "wrong_user", "wrong_password"
  end
  
  def test_root_shows_text_info
    get '/'
    assert last_response.ok?
    assert last_response.body.include?("Available methods")
  end

  def test_invalid_version_shows_404
    get '/1234324'
    assert last_response.status.eql?(404)
  end

  def test_v1_root_shows_version
    get '/1'
    assert last_response.ok?
    assert last_response.body.include?("Version 1.0")
  end

  def test_v2_root_shows_version
    get '/2'
    assert last_response.ok?
    assert last_response.body.include?("Version 2.0")
  end

  def test_default_root_shows_version
#    get '/config/setversion/1'
    set_api_version 1
    get '/'
    assert last_response.ok?
    assert last_response.body.include?("Version 1.0")
  end

  def test_changing_default_version
    get '/'
    assert last_response.ok?
    assert last_response.body.include?("Version 1.0")
#    get '/config/setversion/2'
    set_api_version 2
    get '/'
    assert last_response.ok?
    assert last_response.body.include?("Version 2.0")
  end

  def validate_sites
    assert last_response.ok?
    assert last_response.headers["Content-Type"].include?('application/json')
    x = JSON.parse(last_response.body)
    assert x.include?("1")
    assert x["1"]["name"].eql?('Warszawa')
    assert last_response.body.include?('Kraków')
    assert last_response.body.include?('Gliwice')
  end

  def test_sites_returns_json_with_sites
#    get '/config/setversion/1'
    set_api_version 1
    get '/sites'
    validate_sites
  end

  def test_v1_sites_returns_json_with_sites
    get '/1/sites'
    validate_sites
  end
  
  def test_v2_sites_returns_json_with_sites
    get '/2/sites'
    validate_sites
  end
  
  def validate_site_details(x)
    assert x.include?("id")
    assert x.include?("latitude")
    assert x.include?("longitude")
    assert x.include?("name")
    assert x["name"].eql?('Warszawa')
    assert x.include?("assets")
    assets = x["assets"]
    a0 = assets.first
    assert a0.include?("type")
    assert a0.include?("range")
    assert a0.include?("power")
    assert a0.include?("quantity")
  end
  
  def test_v1_site_returns_json_with_site_details
    get '/1/sites/1'
    assert last_response.ok?
    assert last_response.headers["Content-Type"].include?('application/json')
    x = JSON.parse(last_response.body)
    validate_site_details x
  end
  
  def test_v2_site_returns_json_with_site_details
    get '/2/sites/1'
    assert last_response.ok?
    assert last_response.headers["Content-Type"].include?('application/json')
    x = JSON.parse(last_response.body)
    validate_site_details x
  end
  
  def test_invalid_site_shows_404
    get '/sites/asdfasd'
    assert last_response.status.eql?(404)
  end

  def validate_query_element(x)
    assert x.include?("id")
    assert x.include?("latitude")
    assert x.include?("longitude")
    assert x.include?("name")
    assert x.include?("distance")
    assert x["name"].eql?('Warszawa')
  end
  
  def validate_v1_query
    assert last_response.ok?
    assert last_response.headers["Content-Type"].include?('application/json')
    x = JSON.parse(last_response.body)
    validate_query_element(x)
  end

  def validate_v2_query
    assert last_response.ok?
    assert last_response.headers["Content-Type"].include?('application/json')
    x = JSON.parse(last_response.body)
    validate_query_element(x.first)
    assert x.length == 1
  end

  def test_query_returns_json_with_nearest_site
    set_api_version 1
    get '/query/52.2,21'
    validate_v1_query
  end
  
  def test_v1_query_returns_json_with_nearest_site
    get '/1/query/52.2,21'
    validate_v1_query
  end

  def test_v2_query_returns_json_with_list_of_nearest_sites
    get '/2/query/52.2,21'
    validate_v2_query
  end

  context "Configuration panel" do
    def test_config_without_authentication
      get '/config'
      assert_equal 401, last_response.status
    end

    def test_config_with_bad_credentials
      authorize 'bad', 'boy'
      get '/config'
      assert_equal 401, last_response.status
    end
  
    context "with valid credentials" do
      setup do
        authorize 'admin', 'admin'
      end
      
      def test_config_with_proper_credentials
        get '/config'
        assert_equal 200, last_response.status
        assert last_response.ok?
      end
  
      def test_config_setversion_is_accepted
        get '/config/setversion/1'
        assert last_response.ok?
      end
      
      def test_changing_to_invalid_default_version
        get '/config/setversion/2234234'
        assert last_response.status.eql?(404)
      end
  
      def test_config_setwaittime_is_accepted
        get '/config/setwaittime/1,2'
        assert last_response.ok?
      end
  
      def test_config_reset_is_accepted
        get '/config/reset'
        assert last_response.ok?
      end
  
      def test_config_site_returns_json_with_all_sites_details
        get '/config/allsites'
        assert last_response.ok?
        assert last_response.headers["Content-Type"].include?('application/json')
        x = JSON.parse(last_response.body)
        validate_site_details x["1"]
      end
    end
  end
  
end
