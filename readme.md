# Global Annihilation System Server

**Global Annihilation System Server** is a utility service for FP internal TDD training.

## Running

To run your local copy of GASS you need Ruby 2.1.0 and Bundler to be installed on your system.
First, you should clone this repository. Next, you need to update required dependencies by executing following command in repository folder:

```
$ bundle install
```
When all dependencies are installed you should be able to run local server instance:
```
$ ruby gas_server.rb
```
You can check if it is working by opening your browser and navigating to [http://localhost:4567/](http://localhost:4567/).

Some configuration options are available at [http://localhost:4567/config](http://localhost:4567/config).

To set up password for admin user, first generate salt/hash with:
```
$ ruby generate_password_hash.rb <password>
```
Next use generated commands to configure heroku:
```
$ heroku config:set ADMIN_PASSWORD_SALT=<salt>
$ heroku config:set ADMIN_PASSWORD_HASH=<password hash>
```

Have fun!
