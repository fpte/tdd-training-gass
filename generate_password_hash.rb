#!/usr/bin/env ruby

require 'digest/sha2'
require 'securerandom'

if ARGV.length != 1 then
  puts "Usage: generate_password_hash.rb <password>"
  exit 1
end

password = ARGV[0]
salt = SecureRandom.hex
hash = Digest::SHA512.hexdigest("#{salt}:#{password}")
puts "Salt: #{salt}"
puts "Hash: #{hash}"
puts
puts "heroku config:set ADMIN_PASSWORD_SALT=#{salt}"
puts "heroku config:set ADMIN_PASSWORD_HASH=#{hash}"
