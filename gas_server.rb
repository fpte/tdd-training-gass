# Global Annihilation System Server ;)

require 'json'
require 'sinatra'
require 'slim'
require 'digest/sha2'

# [latitude, longitude, name, [[type, range, power, quantity]]]
set :raw_data, [
  [52.231762, 21.006298, "Warszawa", [
    ["SS-18", 200, 10, 10],
  ]],
  [50.0833300, 19.9166700, "Kraków", [
    ["Minuteman", 200, 50, 10],
    ["Peacekeeper", 30, 0.1, 30]
  ]],
  [50.2833300, 18.6666700, "Gliwice", [
    ["SS-19", 50, 30, 0.3],
    ["DF-5", 100, 60, 0.3],
    ["SM-64", 40, 0.01, 60]
  ]]
]

set :default_version, "1"
set :wait_min_time, 0
set :wait_max_time, 0


helpers do
  def protected!
    return if authorized?
    headers['WWW-Authenticate'] = 'Basic realm="Restricted Area"'
    halt 401, "Not authorized\n"
  end

  def authorized?
    @auth ||=  Rack::Auth::Basic::Request.new(request.env)
    return false unless @auth.provided? and @auth.basic? and @auth.credentials and @auth.credentials.length >= 2
    salt = ENV['ADMIN_PASSWORD_SALT']
    password = @auth.credentials[1]
    hash = ENV['ADMIN_PASSWORD_HASH']
    password_hash = Digest::SHA512.hexdigest("#{salt}:#{password}")
    @auth.credentials[0] == 'admin' and hash == password_hash
  end
end

class Float
  def toRad
    self * Math::PI / 180.0
  end
end

class GASBase
  def initialize(data)
    @data = data
  end
  
  def all_sites
    Hash[@data.map { |id, site| [id, { :name => site[:name] }]}]
  end
  
  def site(id)
    @data[id]
  end
  
  protected
  
  def distance(lat1, lon1, lat2, lon2)
    r = 6371
    dLat = (lat2-lat1).toRad
    dLon = (lon2-lon1).toRad()
    lat1 = lat1.toRad()
    lat2 = lat2.toRad()

    a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    d = r * c
  end

  def query_nearest(latitude, longitude, filter = lambda { |x| true })
    @data.map { |id, site|
      [site, distance(site[:latitude], site[:longitude], latitude, longitude)]
    }.sort_by { |site, dist| dist }.select(&filter).map { |site, dist|
      {:id => site[:id], :latitude => site[:latitude], :longitude => site[:longitude], :name => site[:name], :distance => dist }
    }
  end
end

class GASV1 < GASBase
  def version; "1.0"; end
  
  def description
    "Available methods:\n" +
    "/sites                          - return all sites.\n"+
    "/sites/<id>                     - return site details.\n"+
    "/query/<latitude>,<longitude>   - return nearest site."
  end
  
  def query(latitude, longitude)
    query_nearest(latitude, longitude).first
  end
end

class GASV2 < GASBase
  def version; "2.0"; end
  
  def description
    "Available methods:\n" +
    "/sites                          - return all sites.\n"+
    "/sites/<id>                     - return site details.\n"+
    "/query/<latitude>,<longitude>   - return sites in range."
  end
  
  def query(latitude, longitude)
    filter = lambda { |x|
      site = x[0]
      distance = x[1]
      site[:assets].any? { |a| a[:range] >= distance}
    }
    query_nearest(latitude, longitude, filter)
  end
end


def reset_configuration
  settings.default_version = "1"
  settings.wait_min_time = 0
  settings.wait_max_time = 0
end

set :data, {}
def prepare_data
  index = 1;
  raw_data = settings.raw_data
  settings.data.clear
  raw_data.each do |row|
    settings.data[index.to_s] = {
      :id => index.to_s,
      :name => row[2],
      :latitude => row[0],
      :longitude => row[1],
      :assets => row[3].map { |type, range, power, quantity|
        {
          :type => type,
          :range => range,
          :power => power,
          :quantity => quantity
        }
      }
    }
    index += 1
  end
end
prepare_data

set :services, {}
def initialize_services
  data = settings.data
  settings.services = {
    "1" => GASV1.new(data),
    "2" => GASV2.new(data)
  }
end
initialize_services

# Public API configuration

set :public_folder, File.dirname(__FILE__) + '/static'

def index_page(version)
  service = settings.services[version]
  pass unless service
  slim :index, :locals => { :service => service }
end

def sites_page(version, id)
  service = settings.services[version]
  pass unless service
  content = (id) ? service.site(id) : service.all_sites;
  pass unless content
  content_type :json
  JSON.pretty_generate(content)
end

def allsites_page
  content_type :json
  JSON.pretty_generate(settings.data)
end

def query_page(version, latitude, longitude)
  service = settings.services[version]
  pass unless service
  content_type :json
  JSON.pretty_generate(service.query(latitude.to_f, longitude.to_f))
end

get '/config/?*' do
  # authentication
  protected!
  pass
end

get '/config/setversion/:version' do |version|
  if settings.services.include?(version)
    settings.default_version = version
    message = "Default API version set to " + settings.default_version
  else
    status 404
    message = "Invalid version number!"
  end
  slim :admin_config, :locals => { :message => message }
end

get '/config/setwaittime/:min,:max' do |min_time, max_time|
  settings.wait_min_time = min_time.to_f
  settings.wait_max_time = max_time.to_f
  message = "Response delay set to range #{settings.wait_min_time} - #{settings.wait_max_time}."
  slim :admin_config, :locals => { :message => message }
end

get '/config/reset' do
  reset_configuration
  message = "Configuration reset."
  slim :admin_config, :locals => { :message => message }
end

get '/config' do
  slim :admin_config
end

get '/config/allsites' do
  allsites_page
end

get '/*' do
  if (settings.wait_max_time >= settings.wait_min_time and settings.wait_max_time > 0)
    delay = settings.wait_min_time + rand * (settings.wait_max_time - settings.wait_min_time)
    puts "Delaying request for #{delay} seconds..."
    sleep(delay)
  end
  pass
end

get '/:version?' do |version|
  index_page version || settings.default_version
end

get '/:version?/?sites/?:id?' do |version, id|
  sites_page version || settings.default_version, id
end

get '/:version?/?query/:latitude,:longitude' do |version, latitude, longitude|
  query_page version || settings.default_version, latitude, longitude
end


get '/*' do
  status 404
  slim :page404
end
